package com.univlille.quelqueswidgetssimples;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RatingBar;
import android.widget.TimePicker;
import android.widget.Toast;

/**
 * Exemple de quelques widgets simples avec gestion du clic associé.
 * Vous pouvez aussi jeter un oeil sur le layout pour les contraintes et quelques paramètres
 * utilisés pour les widgets (par exemple la rating bar)
 */

public class MainActivity extends AppCompatActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Gestion d'une case à cocher
        ((CheckBox) findViewById(R.id.CheckBox1)).setOnCheckedChangeListener(new CheckBox.OnCheckedChangeListener() {

            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                afficheToast("Condition A acceptée ? : " + ((isChecked) ? "Oui" : "Non"));
            }
        });

        // Gestion d'une case à cocher
        ((CheckBox) findViewById(R.id.CheckBox2)).setOnCheckedChangeListener(new CheckBox.OnCheckedChangeListener() {

            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                afficheToast("Condition B acceptée ? : " + ((isChecked) ? "Oui" : "Non"));
            }
        });

        // Gestion d'un choix de date
        ((DatePicker) findViewById(R.id.DatePicker01)).init(2018, 10, 29, new DatePicker.OnDateChangedListener() {

            public void onDateChanged(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                afficheToast("La date a changé. Année : " + year + " | Mois : " + monthOfYear + " | Jour : " + dayOfMonth);
            }
        });


        // Gestion d'un vote
        ((RatingBar) findViewById(R.id.RatingBar01)).setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {

            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                afficheToast("Nouvelle note : " + rating);
            }
        });


        // Gestion d'un groupe de boutons radio
        ((RadioGroup) findViewById(R.id.RadioGroup1)).setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

            public void onCheckedChanged(RadioGroup group, int checkedId) {
                afficheToast("Vous avez répondu : " + ((RadioButton) findViewById(checkedId)).getText());
            }
        });

        // Gestion d'un bouton avec image
        findViewById(R.id.ImageButton01).setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                afficheToast("Bouton image cliqué");
            }
        });


        // Gestion d'un choix d'heure
        TimePicker timePicker = findViewById(R.id.TimePicker1);
        timePicker.setOnTimeChangedListener(new TimePicker.OnTimeChangedListener() {
            public void onTimeChanged(TimePicker view, int hourOfDay, int minute) {
                afficheToast("Vous avez choisi : " + hourOfDay + ":" + minute);
            }
        });

    }

    private void afficheToast(String text) {
        Toast.makeText(this, text, Toast.LENGTH_SHORT).show();
    }

}
